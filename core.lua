--- 'Global' values.
local isOSX = system.getInfo( "platform" ) == "macos"
local isWindows = system.getInfo( "platform" ) == "win32"
local isMobile = system.getInfo( "platform" ) == "android" or system.getInfo( "platform" ) == "ios"

--- Required libraries.
local clipboard
local lfs = require( "lfs" )

if isMobile then
	clipboard = require( "plugin.pasteboard" )
elseif isWindows then

	local ok, mod = pcall( require, "plugin.clipboard" )

	if ok then
		clipboard = require( "plugin.clipboard" )
	end

end


-- Localised functions.
local gsub = string.gsub
local execute = os.execute
local chdir = lfs.chdir
local open = io.open
local popen = io.popen
local close = io.close
local remove = os.remove
local time = os.time
local date = os.date
local pathForFile = system.pathForFile

-- Localised values.
local CachesDirectory = system.CachesDirectory

--- Class creation.
local library = {}

--- Initialises this Scrappy library.
-- @param params The params for the initialisation.
function library:init( params )

	-- Get the params, if any
	self._params = params or {}

end

--- Places some text into the clipboard.
-- @param text The text.
-- @return True, and the copied text, if successful, false otherwise.
function library:copy( text )

	-- Do we have some text that is in fact a string?
	if text and type( text ) == "string" then

		if isMobile then
			if clipboard then
				clipboard.copy( "string", text )
			else
				return false
			end
		else
			if isOSX then
				execute( [[echo ']] .. gsub( text, "'", "'\\''" ) .. [[' | pbcopy]] )
			elseif isWindows then
				if clipboard then
					clipboard.SetText( text )
				else
					return false
				end
			end

		end

		return true, text

	end

end

--- Pastes the top-most item on the pasteboard.
-- @param onPaste Listener function to be called when the paste is executed.
-- @return True if successful, false otherwise.
function library:paste( onPaste )

	if isMobile then
		if clipboard then
			clipboard.paste( onPaste )
			return true
		else
			return false
		end
	else
		if isOSX then

			local stamp = time( date('*t') )
		    local filename = "pb_" .. stamp .. ".txt"

		    local current_dir = lfs.currentdir()
		    chdir( pathForFile( "", CachesDirectory ) )
		    execute( "pbpaste > " .. filename )

		    local file, errorString, content = open( pathForFile( filename, CachesDirectory ), "r" )
		    if file then
		        content = file:read( "*a" )
		        close( file )
		    end
		    file = nil

		    remove( pathForFile( filename, CachesDirectory ) )
		    lfs.chdir ( current_dir )

			if onPaste and type( onPaste ) == "function" then
				onPaste{ string = content, url = content }
			end

			return true

		elseif isWindows then

			if clipboard then

				local ok, text = clipboard.GetText()

				if onPaste and type( onPaste ) == "function" then
					onPaste{ success = ok, string = text, url = text }
				end

				return true

			else

				return false

			end

		end

	end

end

--- Destroys the time libray.
function library:destroy()

end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Time library
if not Scrappy.Clipboard then

	-- Then store the library out
	Scrappy.Clipboard = library

end

-- Return the new library
return library
